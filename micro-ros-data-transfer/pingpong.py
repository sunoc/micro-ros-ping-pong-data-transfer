import time
import rclpy
import random
import string
import pandas as pd
from rclpy import qos

from std_msgs.msg import Header

# Receive callback


def sub_cb(msg):
    global msg_received
    msg_received = msg

# Time measure function


def time_measure(node, pub_, N):
    global g_node
    global msg_received
    msg = Header()
    msg.frame_id = ''.join(random.choice(string.ascii_lowercase)
                           for i in range(N))

    # Time measurment starts here
    start = time.perf_counter()

    pub_.publish(msg)
    rclpy.spin_once(node)

    # Time measurment ends here
    finish = time.perf_counter()

    # print("Transmitted message size (bytes):" + str(sys.getsizeof(msg)))
    # print(msg.frame_id)

    if msg.frame_id == msg_received.frame_id:
        return(finish - start)
    else:
        return(-1)

# main function definition


def main(args=None):
    global g_node
    rclpy.init(args=args)
    g_node = rclpy.create_node('minimal_pingpong')

    qos_profile = qos.QoSProfile(
        reliability=qos.QoSReliabilityPolicy.BEST_EFFORT,
        history=qos.QoSHistoryPolicy.KEEP_LAST,
        depth=1
    )

    publisher_ = g_node.create_publisher(
        Header,
        "/microROS/ping",
        qos_profile=qos_profile)

    subscriber_ = g_node.create_subscription(
        Header,
        '/microROS/pong',
        sub_cb,
        qos_profile=qos_profile)
    subscriber_

    df = pd.DataFrame()
    index = 0

    # Measuring 16 and 32 bytes packages for now
    for b in range(16, 33, 16):
        read_data = []

        for n in range(0, 1000):
            ret = time_measure(g_node, publisher_, b)
            if ret == -1:
                print("ERROR: mismatching data received")
            else:
                # print(ret)
                read_data.append(ret/b)

        df.insert(index, str(b), read_data)
        index = index + 1

        time.sleep(0.2)

    print(df)
    df.to_csv('out.csv')

    g_node.destroy_node()
    rclpy.shutdown()

# main function call


if __name__ == '__main__':
    main()
